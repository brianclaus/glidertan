function [] = gtanPlotDEM(gtanDEM,range)

Z=zeros(gtanDEM.mapnx,gtanDEM.mapny);

for i  = 1:gtanDEM.mapny

    Z(:,i) =  gtanDEM.zgridded((gtanDEM.mapnx*(i-1)+1):gtanDEM.mapnx*i);
end

X=gtanDEM.mapx_min+(1:gtanDEM.mapnx).*gtanDEM.mapx_inc;
Y=gtanDEM.mapy_min+(1:gtanDEM.mapny).*gtanDEM.mapy_inc;

figure
clf
h = axes;
set(h,'FontName','Arial','fontsize',12,'LineWidth',1);

contourf(X,Y,Z',range,'linestyle','none')
