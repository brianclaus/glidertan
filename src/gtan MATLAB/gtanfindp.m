
function [indexp] = findp(pp,ckp,Nsp)
    indexp = round(Nsp*pp);
    if(indexp ==0)
        indexp=1;
    end
             %we are too low
	if(ckp(indexp) < pp)
		for i =indexp:Nsp
            if(ckp(i) > pp)
                indexp=i;
                break;
            end
        end
	%we are too high
    else
		for i = (indexp+1) - (1:indexp)  
            if(ckp(i) < pp)
                indexp=i+1;
                break;
            end

        end
    end
    
return;