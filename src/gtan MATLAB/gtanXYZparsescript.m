%%process the gulf data
mapx_inc=0.000833333333333334;
mapy_inc=0.000833333333333334;
mapx_min=-94.500416666666742267;
mapy_min=27.499583333333355333;
mapx_max=-92.999583333333407733;
mapy_max=29.000416666666689867;

load('crm.xyz','ASCII');

gtanDEM = gtanProcessXYZ(crm(:,1),crm(:,2),crm(:,3),mapy_min,mapy_max,mapx_min,mapx_max,mapx_inc,mapy_inc);


gtanPlotDEM(gtanDEM,-200:10:0);

save gtanDEM