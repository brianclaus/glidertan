function gtanDEM = gtanProcessXYZ(X,Y,Z,mapy_min,mapy_max,mapx_min,mapx_max,mapx_inc,mapy_inc)

% lat= zeros(1,length(conceptionbayascii)) ;
% lon= zeros(1,length(conceptionbayascii)) ;
% for i = 1:length(conceptionbayascii)
% [lat(i) lon(i)]=utm2ll(conceptionbayascii(i,1),conceptionbayascii(i,2),22);
% end
% Z=conceptionbayascii(:,3);
% lat = lat;
% lon = lon;
% save ..\..\..\..\Documents\research\cbsmatlab cbs

%load ..\..\..\..\research\cbsmatlab

lon=X;
lat=Y;

index = find(lon > mapx_min & lon < mapx_max & lat > mapy_min & lat < mapy_max);
% figure(1)
% clf
% scatter(lon(index(1:100:length(index))),lat(index(1:100:length(index))),10,Z(index(1:100:length(index))))
% axis equal
mapny = floor((mapy_max-mapy_min)./mapy_inc)+2;
mapnx = floor((mapx_max-mapx_min)./mapx_inc)+2;
% xp= zeros(1,mapny*mapnx);
% yp= zeros(1,mapny*mapnx);
% smoothing=2;
% for i = 1: mapny*mapnx
%    %so we start at xmin,ymin
%    xp(i)=mapx_min+mod(i,mapnx)*mapx_inc;
%    yp(i)=mapy_min + floor(i/mapnx)*mapy_inc; 
% end
%takes forever
% for i = 1: length(zgridded)
%    ind = find(lon(index) > xp(i) - mapx_inc*smoothing & lon(index) < xp(i) + mapx_inc*smoothing & lat(index) > yp(i) - mapy_inc*smoothing & lat(index) < yp(i) + mapy_inc*smoothing);
%    zgridded(i)=mean(Z(ind));
% end
% index=find(zgridded~=-99);
% figure(2)
% clf
% scatter(xp(index),yp(index),10,zgridded(index))
% axis equal
%lets go through and place the depths in the right spot instead of
%searching for depths belonging to spots
%also 
%1)on the parts of the map belonging to the edges which are cut off we
%want there to be -1's
%2)on the parts of the map belonging to shoreline we want -2's

zgridded= zeros(1,mapny*mapnx)-2;
for i = 1:length(Z(index))

    xc=((lon(index(i))-mapx_min)/mapx_inc);
    yc=((lat(index(i))-mapy_min)/mapy_inc);
    
    x1=floor(xc);
    x2=ceil(xc);
    y1=floor(yc);
    y2=ceil(yc);
    
    ind1=((y1)*mapnx+(x1+1));
    ind2=((y1)*mapnx+(x2+1));
    ind3=((y2)*mapnx+(x1+1));
    ind4=((y2)*mapnx+(x2+1));
    if( (lon(index(i))-mapx_min) < 2*mapx_inc || (lat(index(i))-mapy_min) < 2*mapy_inc || (mapx_max-lon(index(i))) < 2*mapx_inc || (mapy_max-lat(index(i))) < 2*mapy_inc )
        if(ind1 > 0 && ind1 < mapny*mapnx)
            zgridded(ind1)=-1;
        end
        if(ind1 > 0 && ind2 < mapny*mapnx)
            zgridded(ind2)=-1;
        end
        if(ind1 > 0 && ind3 < mapny*mapnx)
            zgridded(ind3)=-1;
        end
        if(ind1 > 0 && ind4 < mapny*mapnx)
            zgridded(ind4)=-1;
        end
    else
        if(ind1 > 0 && ind1 < mapny*mapnx)
            if(zgridded(ind1)<0)
                zgridded(ind1)=Z(index(i));
            else
                zgridded(ind1)=(Z(index(i))+zgridded(ind1))/2;
            end
        end
        if(ind1 > 0 && ind2 < mapny*mapnx)
            if(zgridded(ind2)<0)
                zgridded(ind2)=Z(i);
            else
                zgridded(ind2)=(Z(index(i))+zgridded(ind2))/2;
            end
        end
        if(ind1 > 0 && ind3 < mapny*mapnx)
            if(zgridded(ind3)<0)
                zgridded(ind3)=Z(index(i));
            else
                zgridded(ind3)=(Z(index(i))+zgridded(ind3))/2;
            end
        end
        if(ind1 > 0 && ind4 < mapny*mapnx)
            if(zgridded(ind4)<0)
                zgridded(ind4)=Z(index(i));
            else
                zgridded(ind4)=(Z(index(i))+zgridded(ind4))/2;
            end
        end
    end
end

index=find(zgridded~=-1&zgridded~=-2);
% figure(2)
% clf
% scatter(xp(index(1:1:length(index))),yp(index(1:1:length(index))),10,zgridded(index(1:1:length(index))))
% axis equal

gtanDEM.zgridded = zgridded;
gtanDEM.mapnx = mapnx;
gtanDEM.mapny = mapny;
gtanDEM.mapx_inc = mapx_inc;
gtanDEM.mapx_max = mapx_max;
gtanDEM.mapx_min = mapx_min;
gtanDEM.mapy_inc = mapy_inc;
gtanDEM.mapy_max = mapy_max;
gtanDEM.mapy_min = mapy_min;
