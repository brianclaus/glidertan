function glider = gtancorrectAltitudes(glider,svel,dens)


%adjust for roll,pitch and sound velocity error
altc1 = (glider.m_altitude((glider.ia)).*cosd(glider.m_roll((glider.ia))).*cosd(26+glider.m_pitch((glider.ia))));

%adjust the altitude through ray tracing
%the measured glider depth needs to be corrected for density and the
%displacement from the glider frame which is taken as the center of
%buoyancy
xalt=30*0.0254;            %[m] distance to altimeter from CB
xdp = 26*0.0254;           %[m] distance to glider depth sensor from CB
xps = 0.127;            %[m] distance to science pressure from CB
xdpeg = xdp.*sin(-glider.m_pitch.*(pi()/180));
xpseg = xps.*sin(-glider.m_pitch.*(pi()/180));
xalteg = xalt.*sin(-glider.m_pitch.*(pi()/180));

%densa contains the avergage of the water density above the given index 
%mapped to the pressure/10
%depth is equal to (rho*g)/pressure
%xdpc = (densa(round(m_depth)+1).*9.81);
iz = find(~isnan(glider.m_depth));
is = find(~isnan(glider.m_pres));
%xpsc = 100000.*m_pres'./(densa(round(m_pres.*10)+1).*9.81);
dens=[dens dens(100)*ones(1,150)];
xpsc = 100000.*glider.m_pres(is)'./(dens(round(glider.m_pres(is).*10)+1).*9.81);
xdpc = 10000.*glider.m_depth(iz)'./(dens(round(glider.m_depth(iz))+1).*9.81);
xpscg = interp1(glider.m_time(is),xpsc,glider.m_time);
xdpcg = interp1(glider.m_time(iz),xdpc,glider.m_time);


dpscg = xpscg +xpseg;   %science adjusted depth reading to CB
%ddpcg = xdpcg -xdpeg;   %glider adjusted depth reading to CB
ddpcg = xdpcg+xdpeg;

wdc=0;

svel=[svel(1:72) svel(72)*ones(1,150)];


%%seems that raytracing is not strictly necessary for beam bending as the
%%simple water velocity correction gives the same depth estimates however
%%it seems that the horizontal distance and bearing of the return location
%%are significant. perhaps need to consider beam width and reduce angle
%%from vehicle by this?
rolloff =0;
pitchoff =0;
for ii=1:length(glider.iap)
    roll=(glider.m_roll(glider.ap(glider.iap((ii))))+rolloff)*pi()/180;
    pitch=(glider.m_pitch(glider.ap(glider.iap((ii))))+26+pitchoff)*pi()/180;
    theta=acos(cos(roll)*cos(pitch));
    %altimeter depths
    zg=ddpcg((glider.ap(glider.iap((ii)))))'+xalteg((glider.ap(glider.iap((ii)))))';
    zs=dpscg((glider.ap(glider.iap((ii)))))'+xalteg((glider.ap(glider.iap((ii)))))';
    rN=glider.m_altitude(glider.ap(glider.iap((ii))));
    [xout, zout, thetaout] = gtanraytraceglider(roll, pitch, rN, zg, svel);
    glider.water_depthg(ii)=zout;
    altg(ii)=zout-zg+xalteg((glider.ap(glider.iap((ii)))))';
    glider.x_adjg(ii)=xout;%distance in horizontal plane from glider location to water depth location
    theta_adj(ii)=thetaout;%angle between vertical and impact with seafloor
    [xout, zout, thetaout] = gtanraytraceglider(roll, pitch, rN, zs, svel);
    glider.water_depths(ii)=zout;
    glider.x_adjs(ii)=xout;%distance in horizontal plane from glider location to water depth location
    zout = gtanwvelcorrect(rN,zg,svel);
    glider.water_depthw(ii)=zout;
    glider.altw(ii)=zout-zg;
    
end
    

end