%%most variables can be cut from archiving for plotting
%keep them for now

function [glider, gtan] = gtancomputesdxandy(glider,gtan,k)
         %normal ie. no surfacing between alt readings
        %extract sdx, sdy function here
        gtan.DT(k) = glider.time(glider.ia(k))-glider.time(glider.ia(k-1));
        if(glider.i_end(find(glider.i_end>glider.ia(k-1),1))>glider.ia(k))

            iv=glider.ia(k-1)+1:glider.ia(k);
            iv=iv(find(~isnan(glider.speed(iv))));
            dt = glider.time(iv)-glider.time(iv-1);
            dx =(glider.speed(iv).*sin(glider.heading(iv)+glider.headingcor).*dt);
            dy =(glider.speed(iv).*cos(glider.heading(iv)+glider.headingcor).*dt);

            gtan.sdx=sum(dx);
            gtan.sdy=sum(dy);
            gtan.gvel(k)=sum(glider.speed(iv).*dt)/sum(dt);
            
            bearingm = atan2(gtan.sdx,gtan.sdy);
            gtan.sdxu=gtan.sdx;
            gtan.sdyu=gtan.sdy;
            %if there was a surfacing
        else
            igps1=glider.i_end(find(glider.i_end>glider.ia(k-1),1));
            igps2=glider.i_start(find(glider.i_start>glider.ia(k-1),1));
            iv= [glider.ia(k-1)+1:igps1 igps2:glider.ia(k)];
            iapend = glider.iap(k)-(glider.ia(k)-igps2);
            iapc2=(find(glider.ap>igps1,1)-1);
            iapc1=(find(glider.ap>igps2,1)-1);
            DT(k)=glider.time(glider.ia(k))-glider.time(igps2);
            iv=iv(find(~isnan(glider.speed(iv))));
            dt = glider.time(iv)-glider.time(iv-1);
            dx =(glider.speed(iv).*sin(glider.heading(iv)+glider.headingcor).*dt);
            dy =(glider.speed(iv).*cos(glider.heading(iv)+glider.headingcor).*dt);
           

            sdx=sum(dx);
            sdy=sum(dy);
            gtan.gvel(k)=sum(glider.speed(iv).*dt)/sum(dt);
            bearingm = atan2(sdx,sdy);

             [xsurf,ysurf]=gtanlldistkm([glider.yi(iapc2) glider.xi(iapc2) ],[(glider.yi(iapc1)) (glider.xi(iapc1)) ]);
             %we need to update last location with surface drift so our
             %computations don't break
            gtan.sdxu=sdx;
            gtan.sdyu=sdy;
            gtan.sdx=sum(dx) + xsurf;
            gtan.sdy=sum(dy) + ysurf;
        end
        gtan.distu(k) = gtan.distu(k-1)+gtanlldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[(glider.yi(glider.iap(k-1))) (glider.xi(glider.iap(k-1)))]);
        
end