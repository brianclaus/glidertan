%%most variables can be cut from archiving for plotting
%keep them for now

function [glider, gtan] = gtancomputeEstimates(glider,gtan,k)
        
        %we must have good particles
        if(gtan.map==gtan.ded) 
            %sum up the particle weights
            t=sum(gtan.p_zk_given_xk);

            %normalize them
            gtan.wk = gtan.p_zk_given_xk./gtan.po;

%             %find highest weighted particle
%             gtan.wkmax = find(gtan.wk==max(gtan.wk),1);
%               gtan.xhk(1,k)=gtan.xk(1,gtan.wkmax);
%               gtan.xhk(2,k)=gtan.xk(2,gtan.wkmax);
            
            
                %xkm1=xk;
               [gtan.xkt, gtan.wk, gtan.idx]=gtanresample(gtan.xk,gtan.wk','beaglesample'); 
               

            %calc xhk
            %xhk(:,k)=[sum(xk(1,:).*wk);sum(xk(2,:).*wk)];
            for ig = 1:gtan.Ns
               gtan.xhk(1,k)=gtan.xhk(1,k)+gtan.xk(1,gtan.idx(ig))*(1/gtan.Ns);
               gtan.xhk(2,k)=gtan.xhk(2,k)+gtan.xk(2,gtan.idx(ig))*(1/gtan.Ns);
               gtan.xkm1(1,ig)=gtan.xk(1,gtan.idx(ig));
               gtan.xkm1(2,ig)=gtan.xk(2,gtan.idx(ig));
               
            end
             
            gtan.Xn0(k)=gtan.Xn0(k-1)+((gtan.xhk(2,k)-gtan.xhk(2,k-1))/6376669.0)*180/pi();
            gtan.Xe0(k)=gtan.Xe0(k-1)+((gtan.xhk(1,k)-gtan.xhk(1,k-1))/(6376669.0*cosd(gtan.Xn0(k-1))))*180/pi();    
            gtan.xhkdr(:,k)=[gtan.xhkdr(:,k-1)+[gtan.sdx;gtan.sdy]];
            gtan.Xn0dr(k) = gtan.Xn0dr(k-1)+(gtan.sdy/6378137.0)*180/pi();
            gtan.Xe0dr(k) = gtan.Xe0dr(k-1)+(gtan.sdx/(6378137.0*cosd(gtan.Xn0dr(k-1))))*180/pi();
            gtan.wkm1=gtan.wk;
            gtan.var(:,k) = sqrt((gtan.xhk(:,k)-[gtan.xhk(:,k-1)+[gtan.sdxu;gtan.sdyu]]).^2);
            gtan.xdel(:,k) = sqrt((gtan.xhk(:,k)-gtan.xhk(:,k-1)).^2)./gtan.DT(k);
            gtan.gtanvel(k)=sqrt(gtan.xdel(1,k)^2+gtan.xdel(2,k)^2);
            gtan.gtanhead(k)=atan2(gtan.xdel(1,k),gtan.xdel(2,k));
       
            gtan.errkm(k) = gtanlldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0(k) gtan.Xe0(k) ]);
            gtan.errkmdr(k) = gtanlldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0dr(k) gtan.Xe0dr(k) ]);
            gtan.cloudsize(:,k) = mean([sum(sqrt((gtan.xhk(1,k)-gtan.xk(1,gtan.idx)).^2)); sum(sqrt((gtan.xhk(2,k)-gtan.xk(2,gtan.idx)).^2))]);
           gtan.particlecov(:,:,k)=cov(gtan.lono(gtan.idx),gtan.lato(gtan.idx));
           
        else
               % disp(sprintf('Ded reckoning %d...',k))
               gtan.mapbounds(k)=1;
               %update xhk with deadreckoning
               gtan.xhk(:,k) = [gtan.xhk(:,k-1)+[gtan.sdx;gtan.sdy]];
               %update xk with deadreckoning
               gtan.xkm1 = gtan.xk(:,:);          
               gtan.xk = [gtan.xkm1(1,:)+randn(1,gtan.Ns).*2*gtan.R;gtan.xkm1(2,:)+randn(1,gtan.Ns).*2*gtan.R;];     
               gtan.xk = gtan.xk(:,:)+[gtan.sdx.*ones(1,gtan.Ns);gtan.sdy.*ones(1,gtan.Ns)];
               gtan.Xn0(k) = gtan.Xn0(k-1)+(gtan.sdy/6378137.0)*180/pi();
               gtan.Xe0(k) = gtan.Xe0(k-1)+(gtan.sdx/(6378137.0*cosd(gtan.Xn0(k-1))))*180/pi();
               gtan.xhkdr(:,k)=[gtan.xhkdr(:,k-1)+[gtan.sdx;gtan.sdy]];
               gtan.Xn0dr(k) = gtan.Xn0dr(k-1)+(gtan.sdy/6378137.0)*180/pi();
               gtan.Xe0dr(k) = gtan.Xe0dr(k-1)+(gtan.sdx/(6378137.0*cosd(gtan.Xn0dr(k-1))))*180/pi();


                gtan.lato=gtan.Xn0(k-1)+((gtan.xk(2,:)-gtan.xhk(2,k-1))/6376669.0)*180/pi();
                gtan.lono=gtan.Xe0(k-1)+((gtan.xk(1,:)-gtan.xhk(1,k-1))/(6376669.0*cosd(gtan.Xn0(k-1))))*180/pi(); 
               
                gtan.errkm(k) = gtanlldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0(k) gtan.Xe0(k) ]);
                gtan.errkmdr(k) = gtanlldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0dr(k) gtan.Xe0dr(k) ]);
                gtan.var(:,k) = sqrt((gtan.xhk(:,k)-[gtan.xhk(:,k-1)+[gtan.sdxu;gtan.sdyu]]).^2);
                gtan.xdel(:,k) = sqrt((gtan.xhk(:,k)-gtan.xhk(:,k-1)).^2)./gtan.DT(k);
                gtan.gtanvel(k)=sqrt(gtan.xdel(1,k)^2+gtan.xdel(2,k)^2);
            gtan.gtanhead(k)=atan2(gtan.xdel(1,k),gtan.xdel(2,k));
                %check what the deal is here with the cov
                gtan.cloudsize(:,k) = mean([sum(sqrt((gtan.xhk(1,k)-gtan.xk(1,:)).^2)); sum(sqrt((gtan.xhk(2,k)-gtan.xk(2,:)).^2))]);
                gtan.particlecov(:,:,k)=cov(gtan.lono,gtan.lato);

            gtan.map=3;
        end
end