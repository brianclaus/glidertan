%in this one we just bin the altimeter reading and correct the distance by
%the sound velocity in each bin without considering direction
function [zout] = gtanwvelcorrect(altitude, depth, svel)

%ii=5;
i=1;


%theta=pitch;
z=depth;
t=0;

rN=altitude;
tN=rN/1500;
dz=.1;
c=svel(floor(z)+1)+(svel(ceil(z)+1)-svel(floor(z)+1))*((z-floor(z))/(ceil(z)-floor(z)));


while(t(i)<tN)
z(i+1)=z(i)+dz;
dt=dz/c(i);
t(i+1)=t(i)+dt;
c(i+1)=svel(floor(z(i+1))+1)+(svel(ceil(z(i+1))+1)-svel(floor(z(i+1))+1))*((z(i+1)-floor(z(i+1)))/(ceil(z(i+1))-floor(z(i+1))));
i=i+1;
end
zout = z(i);
