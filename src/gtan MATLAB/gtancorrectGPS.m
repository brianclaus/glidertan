function glider = gtancorrectGPS(glider)


    %% Compute the corrected gps locations
    % glider.dr_state
    % out, mission_start=0, underwater=1,awaiting_fix=2,
    %      awaiting_postfix=3, awaiting_dive=4
    %i_search = find(~isnan(glider.dr_state));

    %dr dive location
    %lat/glider.lon index for transition from 4->1
    %convert to function



    %gps location at surface
    %transition from 2->3
    i_end = find(diff(glider.dr_state.^2)==5);
    for ki = 1:length(i_end)
    %make sure we're on a location
         while(isnan(glider.lon(i_end(ki))) && isnan(glider.gps_lon(i_end(ki))))
             i_end(ki) = i_end(ki) +1;
         end
    end

    %DR location after surfacing
    %transition from 1->2
    %make sure we're on a location
    i_mid = find(diff(glider.dr_state.^2)==3);
    for ki = 1:length(i_mid)
    %make sure we're on a location
         while(isnan(gtantoDD(glider.lon(i_mid(ki)))))
             i_mid(ki) = i_mid(ki) -1;
         end
    end  

    %sometimes we start and don't finsh due to an abort
    %need to remove these segments
    i_start= find(diff(glider.dr_state.^2)==-15);

    for ki = 1:length(i_end)-1
         if( i_end(ki)-i_start(ki)>i_start(ki+1)-i_start(ki))
            i_start = [i_start(1:ki-1); i_start(ki+1:end)];
         end     
    end
    
    for ki = 1:length(i_start)    
    %make sure we're on a location    
         while(isnan(glider.lon(i_start(ki))))
             i_start(ki) = i_start(ki) +1;
         end
    end


    lon_dif = gtantoDD(glider.lon(i_end))-gtantoDD(glider.lon(i_mid));
    lat_dif = gtantoDD(glider.lat(i_end))-gtantoDD(glider.lat(i_mid));
    t_dif = glider.time(i_mid)-glider.time(i_start);

    vlonDD=lon_dif./t_dif;
    vlatDD=lat_dif./t_dif;
    glider.xi=gtantoDD(glider.lon(1));
    glider.yi=gtantoDD(glider.lat(1));
    ap=1;

    for h=1:length(i_start)
        a = i_start(h)+(find(~isnan(glider.lon(i_start(h):i_mid(h)))))-1;
        ap = [ap;a];
        ti = glider.time(a) - glider.time(a(1));
        glider.xi = [glider.xi; (gtantoDD(glider.lon(a)) + ti.*vlonDD(h))];
        glider.yi = [glider.yi; (gtantoDD(glider.lat(a)) + ti.*vlatDD(h))];
    end



    iap = find(~isnan(glider.water_depth(ap)) & glider.m_pitch(ap) < -15 );
    iap = iap(find(diff([glider.m_time(ap(iap(1)));glider.m_time(ap(iap))])<1200));
    glider.ap=ap;

     start=1;
     glider.iap=iap(start:length(iap));
     glider.ia = ap(iap);
     glider.i_end=i_end;
     glider.i_start=i_start;
    

end