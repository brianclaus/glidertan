function [xout zout thetaout] = raytracingtest(roll, pitch, altitude, depth, svel)

%ii=5;
i=1;


theta=acos(cos(roll)*cos(pitch));
%theta=pitch;
z=depth;
x=0;
r=0;
t=0;

rN=altitude;
tN=rN/1500;
dz=.1;
c=svel(floor(z)+1)+(svel(ceil(z)+1)-svel(floor(z)+1))*((z-floor(z))/(ceil(z)-floor(z)));
a=sin(theta)/c;

while(t(i)<tN)
z(i+1)=z(i)+dz;
dx=dz*tan(theta(i));
dr=sqrt(dx^2+dz^2);
dt=dr/c(i);
x(i+1)=x(i)+dx;
r(i+1)=r(i)+dr;
t(i+1)=t(i)+dt;
c(i+1)=svel(floor(z(i+1))+1)+(svel(ceil(z(i+1))+1)-svel(floor(z(i+1))+1))*((z(i+1)-floor(z(i+1)))/(ceil(z(i+1))-floor(z(i+1))));
theta(i+1) = asin(a*c(i+1));
i=i+1;
end
xout = x(i);
zout = z(i);
thetaout = theta(i);
