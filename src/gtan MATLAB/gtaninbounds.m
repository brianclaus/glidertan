function out=gtaninbounds(x,y,gtan)

mapy_min=gtan.mapy_min;
mapx_max=gtan.mapx_max;
mapx_min=gtan.mapx_min;
mapy_max=gtan.mapy_max;

%check x
if(x < mapx_min || x > mapx_max)
    out = 0;
%else check y
elseif(y > mapy_max || y < mapy_min)
    out =0;
    
else%in bounds
    out = 1;
end


end