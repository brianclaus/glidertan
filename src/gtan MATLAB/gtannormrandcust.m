function o=normrandcust(m,s)

r1 = rand();
r2 =rand();
o = m + s * sqrt(-2*log(r1))*cos(2*pi*r2);
end