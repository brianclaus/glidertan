function glider = gtanvariables(data,sensor_lookup,i)

    glider.lon = data(i,sensor_lookup.m_lon);
    glider.lat = data(i,sensor_lookup.m_lat);
    glider.wpt_lon = data(i,sensor_lookup.c_wpt_lon);
    glider.wpt_lat = data(i,sensor_lookup.c_wpt_lat);
    glider.gps_lat = data(i,sensor_lookup.m_gps_lat);
    glider.gps_lon = data(i,sensor_lookup.m_gps_lon);
    glider.speed = data(i,sensor_lookup.m_speed);
    glider.heading = data(i,sensor_lookup.m_heading);
    glider.dr_state = data(i,sensor_lookup.x_dr_state);
    glider.m_altitude = data(i,sensor_lookup.m_altitude);
    glider.m_depth = data(i,sensor_lookup.m_depth);
    glider.m_pitch = data(i,sensor_lookup.m_pitch).*180/pi();
    glider.m_roll = data(i,sensor_lookup.m_roll).*180/pi();
    glider.m_ballast = data(i,sensor_lookup.m_ballast_pumped);
    glider.m_battpos = data(i,sensor_lookup.m_battpos);
    glider.m_pres = data(i,sensor_lookup.m_water_pressure);


    glider.time = data(i,sensor_lookup.m_present_time) - data(1,sensor_lookup.m_present_time);
    glider.m_time = data(i,sensor_lookup.m_present_time);


    glider.water_depth = glider.m_altitude+glider.m_depth;
    glider.headingcor = -pi()*(18/180);

end