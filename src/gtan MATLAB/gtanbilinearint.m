function zI=gtanbilinearint(x,y,gtan)

mapy_min=gtan.mapy_min;
mapx_max=gtan.mapx_max;
mapx_min=gtan.mapx_min;
mapx_inc=gtan.mapx_inc;
mapy_inc=gtan.mapy_inc;
map=gtan.zgridded;

mapnx = floor((mapx_max-mapx_min)./mapx_inc)+2;

 xp=((x-mapx_min)/mapx_inc);
yp=((y-mapy_min)/mapy_inc);
x1=floor(xp);
x2=ceil(xp);
y1=floor(yp);
y2=ceil(yp);
ind=((y1)*mapnx+(x1+1));
z11=map(ind);
ind=((y1)*mapnx+(x2+1));
z21=map(ind);
ind=((y2)*mapnx+(x1+1));
z12=map(ind);
ind=((y2)*mapnx+(x2+1));
z22=map(ind);
x1=mapx_min+floor(xp)*mapx_inc;
x2=mapx_min+ceil(xp)*mapx_inc;
y1=mapy_min+floor(yp)*mapy_inc;
y2=mapy_min+ceil(yp)*mapy_inc;
if(z11==-1||z12==-1||z21==-1||z22==-1)
    zI=-1;
else
    zI=(1/((x2-x1)*(y2-y1)))*(z11*(x2-x)*(y2-y)+z21*(x-x1)*(y2-y)+z12*(x2-x)*(y-y1)+z22*(x-x1)*(y-y1));
end
end