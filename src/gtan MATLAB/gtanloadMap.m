function gtan = gtanloadMap(gtan,name)

load(name)


gtan.mapy_min=mapy_min;
gtan.mapy_max=mapy_max;
gtan.mapx_max=mapx_max;
gtan.mapx_min=mapx_min;
gtan.mapx_inc=mapx_inc;
gtan.mapy_inc=mapy_inc;
gtan.zgridded=zgridded;

end