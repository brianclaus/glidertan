%%most variables can be cut from archiving for plotting
%keep them for now

function [glider, gtan] = gtancomputeParticlesandWeights(glider,gtan,k)
        gtan.po=0;
        for i = 1:gtan.Ns
            %break this core loop into function
            
            %we were ded-reckoning
            %lets reset our priors and start again
            if(gtan.map==3)                
                %lets try again
                gtan.map=0;
                
            end
            %propose distance traveled metric
            %so jitter based on an error per distance traveled of say
%             %25-50%
             gtan.R = 0.25*(sqrt(gtan.sdxu^2+gtan.sdyu^2));

          %  alternatively we could compute the difference between the
          %  dead-reckoned speed through water and the gtan speed over
          %  ground to estimate the error rate
            %gtan error rate
%            
%            if(k<15)
%                 gtan.R = 0.25*(sqrt(gtan.sdxu^2+gtan.sdyu^2));
%            else
%                 gtan.drerrrate(k)=abs(1-gtan.gtanvel(k-1)/gtan.gvel(k-1));
%                 gtan.R = mean(gtan.drerrrate(k-10:k))*(sqrt(gtan.sdxu^2+gtan.sdyu^2));
%            end

            if(gtan.map==gtan.ded)

                sigmaz = 0.5;
     
                        rk(1) = gtannormrandcust(0,gtan.R);
                        rk(2) = gtannormrandcust(0,gtan.R);
                   
                    x1 = (glider.water_depthg(k)-glider.m_depth(glider.ap(glider.iap(k))))*tan((glider.m_pitch(glider.ap(glider.iap(k)))+26)*pi()/180);
                    a1 = sqrt(2*glider.m_altitude(glider.ap(glider.iap(k)))^2-2*glider.m_altitude(glider.ap(glider.iap(k)))^2*cos((glider.m_roll(glider.ap(glider.iap(k))))*pi()/180));
                    
                    A1=atan2(a1,x1);
                    
                    xp=glider.x_adjg(k).*sin(glider.heading(glider.ap(glider.iap(k)))+glider.headingcor+A1);
                    yp=glider.x_adjg(k).*cos(glider.heading(glider.ap(glider.iap(k)))+glider.headingcor+A1);
% % 
%                      gtan.xk(1,i) = rk(1)+gtan.xkm1(1,i)+gtan.sdx-xp;
%                      gtan.xk(2,i) = rk(2)+gtan.xkm1(2,i)+gtan.sdy-yp;  

                   gtan.xk(1,i) = rk(1)+gtan.xkm1(1,i)+gtan.sdx;
                   gtan.xk(2,i) = rk(2)+gtan.xkm1(2,i)+gtan.sdy;    
                   %compute lat and lon of each particle
                   %use last lat and lon and Dx,Dy to compute new positions
                    gtan.lato(i)=gtan.Xn0(k-1)+((gtan.xk(2,i)-gtan.xhk(2,k-1))/6376669.0)*180/pi();
                    gtan.lono(i)=gtan.Xe0(k-1)+((gtan.xk(1,i)-gtan.xhk(1,k-1))/(6376669.0*cosd(gtan.Xn0(k-1))))*180/pi(); 


               %check if we are outside of map bounds and we have an altimeter
               %reading        
                if(gtaninbounds(gtan.lono(i),gtan.lato(i),gtan))
     
                        zI=gtanbilinearint(gtan.lono(i),gtan.lato(i),gtan);
                        %we are leaving the map bounds
                        if(zI==-1)
                            gtan.mapbounds(k)=1;
                            gtan.map=1;
                        else
                            %we are near the shoreline
                            if(zI==-2)
                                gtan.shoreline(k)=1;
                            end
                            hhzkbl = zI;
                            
                            %hydrographic uncertainty model from IHO
                            %special survey
                            %+-sqrt(a^2+(b*d)^2))
                            a=0.25;
                            b=0.0075;
                            sigmah=sqrt(a^2+(b*gtan.water_depthe(k))^2);
                            
                            %altimeter uncertainty model
                            sigmat=(0.02*glider.altw(k))^2;
                            %pressure sensor uncertainty model
                            sigmaz=(0.001*(gtan.water_depthe(k)-glider.altw(k)))^2;
                     
                            gtan.p_zk_given_xk(i)=normpdf(gtan.water_depthe(k),hhzkbl,sigmah);
                            % weights 
                            if(abs(gtan.p_zk_given_xk(i)) < normpdf(0,5*sigmah,sigmah))
                                gtan.p_zk_given_xk(i) = normpdf(0,5*sigmah,sigmah);
                            end
                       % end
                            gtan.po = gtan.po+ gtan.p_zk_given_xk(i);
                        end
                       
                else
                    %oh nos we are outside our map what to do?
                    %we revert to ded-reckoning
                                  
                     gtan.map=1;
                    %k
                end
            end
    
        end

end