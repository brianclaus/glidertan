#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "normstat.h"
#include "mtrand.h"
#include "common.h"

//error codes
#define ESTIMATE_GOOD		0
#define ESTIMATE_OFF_MAP 	1
#define ESTIMATE_WAS_DR		2

/*
ver 0.1 bclaus@mun.ca 23/04/2014
Initial Build
Supports init/dataIn commands
barebones functioning with data being sent through the ftdi serial connection ttyO0
see gTANinitcommands.txt for an example of the initialisation procedure
all constants and filenames are presently compiled in through common.h

TODO:
reset command to be implemented
variable structure to send back to glider needs to be implemented

*/
double version = 0.1;


int getGliderData (FILE *ptr);
int	getGliderDataSerial(void);
int getGPSpos (FILE *ptr);
bool areweinthemapbounds(double x, double y);
double bilinearmap(double *ptr,double x, double y);
double lldist(double lat1, double lon1,double lat2,double lon2);
void resample(double *rk, int *idx, int Ns);
int findp(double p, double *wk,int Ns);
void waitForC(void);
int raytracedwaterdepth(double roll,double pitch,double heading,double altitude,double depth);
double toDD(double latorlon);


int main(){
	//Setup Variables
	//mostly moved to common.h
	int err_pf=ESTIMATE_GOOD;//no error
	double rk1=0,rk2=0;
	double hhz=0;
	double sigmah=0;
	char dataToGlider [80];
	double sumlono=0,sumlato=0;
	double meanlono=0,meanlato=0;
	double sumx1=0,sumx2=0;
	double meanx1=0,meanx2=0;

	//Open files
    ////printf("open files\n\r");
	//FILE * fdataIn;
	//fdataIn = fopen ("datatobbb", "r");
	
	//redirect stdout to log file
	//freopen ("gtanlogs","a",stdout);
	
	FILE * fmapData;
	fmapData = fopen ("hroodgridmap2", "rb");

	//FILE * gpspos;
	//gpspos = fopen ("gpscortobbb", "r");

	FILE * sveldata;
	sveldata = fopen ("sveltobbb", "r");

	//Open Comms to glider
    //printf("open comms\n\r");
	FILE * gHandle;
    gHandle = fopen("/dev/ttyO1","r+");

	//fprintf(gHandle,"Comms are now  online");
	//read in map to ram
	//read in file
    //printf("read in map\n\r");
	// obtain file size:
	fseek (fmapData , 0 , SEEK_END);
	lSize = ftell (fmapData);
	rewind (fmapData);
	// allocate memory to contain the whole file:
	mapBuffer = (double*) malloc (lSize);
	if (mapBuffer == NULL) {fputs ("Memory error",stderr); exit (2);}
	// copy the file into the buffer:
	result = fread (mapBuffer,4,lSize,fmapData);
	if (result != lSize/4) {
		////printf("result is %u and lsize is %lu",result,lSize);

		fputs ("Reading error",stderr); exit (3);
		}
	/* the whole file is now loaded in the memory buffer. */
	fclose(fmapData);

	//readin svel profile to ram
    ////printf("read in svel\n\r");
	int size;
	int i=0;
	double svelTemp;
	while(size!=EOF){
		size = fscanf(sveldata,"%lf \n", &svelTemp);
		svelBuffer[i]=svelTemp;
		i++;
	}



	//setup rand number gen

	init_genrand(clock());

	//main loop
	//what we do
	//the main loop is structured as a mini-OS which responds to certain commands
	//commands we have
	//init - this runs at the start of a mission
	//dataIn - this is run when there is a new altimeter hit which is sent over from the glider
	//reset - this resets the particle filter



	

	 if (gHandle!=NULL)
    {
		do{
			gHandle = freopen("/dev/ttyO1","w",gHandle);
		}while(gHandle==NULL);					
		sprintf(dataToGlider,"boardBooted\n");
		fputs(dataToGlider,gHandle);
			
		do{
			gHandle = freopen("/dev/ttyO1","r",gHandle);
		}while(gHandle==NULL);
	}
	

	
    //wait until we have an initialisation commmand
/*
    do{
		////printf("command?:");
    	fgets(command,80,gHandle);
    }while(strcmp(commandInit,command)!=0);
	//printf("wait for some inits\n\r");
    //scan for the initialisation parameters
	fscanf(gHandle,"%lf %lf %lf %lf %lf %lf",&Xn0,&Xe0,&x_lmc_from_glider,&y_lmc_from_glider,&depth_bias,&jitter);
	Xn0 = toDD(Xn0);
	Xe0 = toDD(Xe0);
	
	//try to handshake the init
	do{
		gHandle = freopen("/dev/ttyO1","w",gHandle);
	}while(gHandle==NULL);					
			
	sprintf(dataToGlider,"eh?\n");
	fputs(dataToGlider,gHandle);
	sprintf(dataToGlider,"%lf  tInited\n",Xn0+Xe0+x_lmc_from_glider+y_lmc_from_glider+depth_bias+jitter);
	fputs(dataToGlider,gHandle);
	//time(&timer1);
	//wait a sec
	//while(difftime(timer1+1,time(&timer2))>0);
	//fflush(gHandle);
	//fclose(gHandle);
	do{
		gHandle = freopen("/dev/ttyO1","r",gHandle);
	}while(gHandle==NULL);
	
*/
    //printf("gtan inited with %f,%f,%f,%f,%f,%f\n\r",Xn0,Xe0,x_lmc_from_glider,y_lmc_from_glider,depth_bias,jitter);

	//rotate by magdec	
	xhk1=x_lmc_from_glider*cos(magdeclination)+y_lmc_from_glider*sin(magdeclination);
	xhk2=-x_lmc_from_glider*sin(magdeclination)+y_lmc_from_glider*cos(magdeclination);
	x_lmc_from_glider_last = x_lmc_from_glider;	
	y_lmc_from_glider_last = y_lmc_from_glider;	
	
	for(int i=0; i < Ns; i++) {
		xkm1[i] = xhk1;
		xkm2[i] = xhk2;
	}
	//assume lmc is inited to here
	//in reality this is the diving location?
	//initialise arrays to non zero values using the syntax below
	//int a[] = { [0 ... 9] = 1, [10 ... 99] = 2, [100] = 3 };
	
	
	newData=switchwait;
    //printf("run particle filter\n\r");
	while(1){
		switch (newData){
		case switchrun:
			//lets get our data then!
			do{
				gHandle = freopen("/dev/ttyO1","r",gHandle);
			}while(gHandle==NULL);	
		    size = fscanf(gHandle,"%lf  %lf  %lf  %lf  %lf  %lf  %lf  %lf %lu", &x_lmc_from_glider,
														&y_lmc_from_glider,
														&alt_from_glider,
														&depth_from_glider,
														&roll_from_glider,
														&pitch_from_glider,
														&heading_from_glider,
														&index_from_glider,
														&checksum);
		//valid data?
			if(checksum == (unsigned long)(x_lmc_from_glider*256)+
						(unsigned long)(y_lmc_from_glider*256)+
						(unsigned long)(alt_from_glider*256)+
						(unsigned long)(depth_from_glider*256)+
						(unsigned long)(roll_from_glider*256)+
						(unsigned long)(pitch_from_glider*256)+
						(unsigned long)(heading_from_glider*256)+
						(unsigned long)(index_from_glider*256)){
				//yup lets keep going and tell the glider!
				do{
					gHandle = freopen("/dev/ttyO1","w",gHandle);
				}while(gHandle==NULL);	
				sprintf(dataToGlider,"y\n");
				fputs(dataToGlider,gHandle);
				do{
					gHandle = freopen("/dev/ttyO1","r",gHandle);
				}while(gHandle==NULL);			
			}
			else
			{
				//nope we need to try again
				do{
					gHandle = freopen("/dev/ttyO1","w",gHandle);
				}while(gHandle==NULL);	
				sprintf(dataToGlider,"n\n");
				fputs(dataToGlider,gHandle);
				do{
					gHandle = freopen("/dev/ttyO1","r",gHandle);
				}while(gHandle==NULL);
				newData=switchrun;
				break;
			
			}

		
		
		
			//estimate_water_depth();
			//water_depthe = alt_from_glider+depth_from_glider;
			// //printf("%.8f %.8f %.8f %.8f \n\r",roll_from_glider*pi/180,pitch_from_glider*pi/180+26*pi/180, alt_from_glider, depth_from_glider);
			 if(raytracedwaterdepth(roll_from_glider, pitch_from_glider+0.454, heading_from_glider,alt_from_glider, depth_from_glider)){
				water_depthe=raytracedz;
				
			 }
			 else{
				//something went really wrong
				water_depthe=0;
				err_pf = ESTIMATE_OFF_MAP;
			 }

			////printf("%.8f %.8f\n\r",alt_from_glider+depth_from_glider,water_depthe);
				//waitForC();
			//extract sdx/sdy from data from glider
			sdx=x_lmc_from_glider-x_lmc_from_glider_last;
			sdy=y_lmc_from_glider-y_lmc_from_glider_last;
			x_lmc_from_glider_last=x_lmc_from_glider;
			y_lmc_from_glider_last=y_lmc_from_glider;
			//that was in glider frame
			//our map is in inertial frame
			//we need to rotate by mag decl  magdeclination
			sdxR = sdx*cos(magdeclination) + sdy*sin(magdeclination);
			sdyR = -sdx*sin(magdeclination) + sdy*cos(magdeclination);
			
			//do the gTAN algorithm

			//generate_particles_and_weights();
			po=0;
			sumlono=0;
			sumlato=0;
			sumx1=0;
			sumx2=0;
			for(int i=0;i<Ns;i++){
				if(err_pf==ESTIMATE_WAS_DR){

					//try try again
					err_pf=ESTIMATE_GOOD;
				}
				if(err_pf==ESTIMATE_GOOD){
					//compute ded-reck distance
					//generate particle deviations
					rk1=normrand(0,jitter);
					rk2=normrand(0,jitter);

					//generate particles
					xk1[i] = sdxR + xkm1[i] + rk1-raytracedx_off;
					xk2[i] = sdyR + xkm2[i] + rk2-raytracedy_off;

					//compute lat and lon of each particle
					lato[i]=Xn0+((xk2[i]-xhk2)/6376669)*180/pi;
					lono[i]=Xe0+((xk1[i]-xhk1)/(6376669*cos(Xn0*pi/180)))*180/pi;

					sumlato= sumlato+lato[i];
				    sumlono= sumlono+lono[i];
					sumx1= sumx1+xk1[i];
				    sumx2= sumx2+xk2[i];

					//compute weight if in map bounds
					if(areweinthemapbounds(lono[i],lato[i])){

						hhz=bilinearmap(mapBuffer,lono[i],lato[i]);
						//map bounds are rectangular so we do additional checks
						//are we leaving a map boundary
						if(hhz==-1){
							err_pf = ESTIMATE_OFF_MAP;
						}
						



						sigmah=mapvariance*sqrt(1+pow(0.023*water_depthe,2));

						p_zk_given_xk[i]=normpdf(water_depthe-depth_bias,hhz,sigmah);
						if(p_zk_given_xk[i] < normpdf(0,5*sigmah,sigmah)){
							 p_zk_given_xk[i] = normpdf(0,5*sigmah,sigmah);

						}
						//compute sum
						//calculate_total_weight();
						po = po + p_zk_given_xk[i];
					}
					//else throw out of bounds error and break
					else{
						err_pf = ESTIMATE_OFF_MAP;
					}//if(areweinthemapbounds(lato[i],lono[i]))
				}//if(err_pf==ESTIMATE_GOOD)
			}//for

			if(err_pf==ESTIMATE_GOOD){

				//normalize_weights();
				for(int i=0;i<Ns;i++){
					wk[i] = p_zk_given_xk[i]/po;
				}
				//resample();
				resample(wk,resample_index,Ns);


				//estimate_state();
				/*for ig = 1:Ns
				   xhk(1,k)=xhk(1,k)+xk(1,idx(ig))*(1/Ns);
				   xhk(2,k)=xhk(2,k)+xk(2,idx(ig))*(1/Ns);
				   xkm1(1,ig)=xk(1,idx(ig));
				   xkm1(2,ig)=xk(2,idx(ig));
				end*/
				//and calc the covariance matrix values				
				meanlato= sumlato/Ns;
				meanlono= sumlono/Ns;			
				meanx1= sumx1/Ns;
				meanx2= sumx2/Ns;
				xhk1m1=xhk1;
				xhk2m1=xhk2;
				xhk1 =0;
				xhk2 =0;
				covlatlat=0;
				covlonlon=0;
				covlatlon=0;
				covx1x1=0;
				covx2x2=0;
				covx1x2=0;
				for(int i=0;i<Ns;i++){
					xhk1 += xk1[resample_index[i]]*((double)1/Ns);
					xhk2 += xk2[resample_index[i]]*((double)1/Ns);
					xkm1[i]=xk1[resample_index[i]];
					xkm2[i]=xk2[resample_index[i]];
					
					covlatlat=covlatlat+(lato[i]-meanlato)*((lato[i]-meanlato));
					covlonlon=covlonlon+(lono[i]-meanlono)*((lono[i]-meanlono));
					covlatlon=covlatlon+(lato[i]-meanlato)*((lono[i]-meanlono));
					covx1x1=covx1x1+(xk1[i]-meanx1)*((xk1[i]-meanx1));
					covx2x2=covx2x2+(xk2[i]-meanx2)*((xk2[i]-meanx2));
					covx1x2=covx1x2+(xk1[i]-meanx1)*((xk2[i]-meanx2));
				}

				covlatlat=covlatlat/Ns;
				covlonlon=covlonlon/Ns;
				covlatlon=covlatlon/Ns;	
				covx1x1=covx1x1/Ns;
				covx2x2=covx2x2/Ns;
				covx1x2=covx1x2/Ns;	
				
				Xn0m1=Xn0;
				Xe0m1=Xe0;
				//Xn0(k)=Xn0(k-1)+((xhk(2,k)-xhk(2,k-1))/6376669.0)*180/pi();
				//Xe0(k)=Xe0(k-1)+((xhk(1,k)-xhk(1,k-1))/(6376669.0*cosd(Xn0(k-1))))*180/pi();
			    //newsdx=(xhk1-xhk1m1)*cos(-magdeclination)-(xhk2-xhk2m1)*sin(-magdeclination);
			    //newsdy=(xhk1-xhk1m1)*sin(-magdeclination)+(xhk2-xhk2m1)*cos(-magdeclination);
				Xn0=Xn0m1+((xhk2-xhk2m1)/6376669)*180/pi;
				Xe0=Xe0m1+((xhk1-xhk1m1)/(6376669*cos(Xn0m1*pi/180)))*180/pi;
				
				
				

			}//if(err_pf==ESTIMATE_GOOD)
			else if(err_pf==ESTIMATE_OFF_MAP){

				xhk1m1=xhk1;
				xhk2m1=xhk2;
				Xn0m1=Xn0;
				Xe0m1=Xe0;
				xhk1=xhk1m1+sdxR;
				xhk2=xhk2m1+sdyR;
				for(int i=0;i<Ns;i++){
					rk1=normrand(0,jitter);
					rk2=normrand(0,jitter);
					xkm1[i]=xhk1+rk1;
					xkm2[i]=xhk2+rk2;
				}
				//compute lat and lon of estimate
				Xn0=Xn0m1+(sdyR/6376669)*180/pi;
				Xe0=Xe0m1+(sdxR/(6376669*cos(Xn0m1*pi/180)))*180/pi;
				err_pf=ESTIMATE_WAS_DR;
			}//else if(err_pf==ESTIMATE_OFF_MAP)
			//send data to the glider here
			//fclose(gHandle);
			//gHandle = fopen("/dev/ttyO1","w");
			//fseek(gHandle,0,SEEK_CUR);
			
			//this sdx/sdy needs to be the difference from the transmitted locations from the glider
			//it is an update to current position not an update from the last position
			sdxToGlider=((xhk1)*cos(magdeclination)-(xhk2)*sin(magdeclination))-x_lmc_from_glider;
			sdyToGlider=((xhk1)*sin(magdeclination)+(xhk2)*cos(magdeclination))-y_lmc_from_glider;
			newData = switchsend;
			/*
			sprintf(dataToGlider,"D %lf %lf %lf %lf %lf %lf %d %lf\r",sdxToGlider,sdyToGlider,Xn0,Xe0,
			((xhk1)*cos(magdeclination)-(xhk2)*sin(magdeclination)),
			((xhk1)*sin(magdeclination)+(xhk2)*cos(magdeclination)),
			err_pf,index_from_glider);
			
			fputs(dataToGlider,gHandle);
			//printf("the datas to the gliders: %s\r",dataToGlider);

			do{
				gHandle = freopen("/dev/ttyO1","r",gHandle);
			}while(gHandle==NULL);
	
			newData=switchwait;
			*/
			break;
		case switchwait:

		
			//Now we query glider for the next command
			do{
				/*do{
					gHandle = freopen("/dev/ttyO1","w",gHandle);
				}while(gHandle==NULL);
				sprintf(dataToGlider,"?\n");			
				fputs(dataToGlider,gHandle);	
				do{
					gHandle = freopen("/dev/ttyO1","r",gHandle);
				}while(gHandle==NULL);*/
				fgets(command,256,gHandle);
				
			}while(strcmp(commandDataIn,command)!=0&&
				strcmp(command,commandInit)!=0&&
				strcmp(command,commandReset)!=0);
			//printf("post %s\n\r",command);
			
			//if we are actually navigating do this one
			if(strcmp(command,commandDataIn)==0){

				newData=switchrun;	
			}
			else if(strcmp(command,commandInit)==0){
			
				newData=switchinit;	
				
			}
			else if(strcmp(command,commandReset)==0){
			
				newData=switchreset;	
				
			}
			else{
				newData=switchwait;
			}
			
			
			break;
		case switchinit:
		
			//if we get a new init command that means we have new gps updates. woot woot
			//reset the lat0/lon0 and the lmc values
			//fscanf needs a ^M to terminate (carraige return)
			fscanf(gHandle,"%lf %lf %lf %lf %lf %lf %lu",&Xn0,&Xe0,&x_lmc_from_glider,&y_lmc_from_glider,&depth_bias,&jitter,&checksum);
			//checksum valid?	
			if(checksum == (unsigned long)(Xn0*256)+
							(unsigned long)(Xe0*256)+
							(unsigned long)(x_lmc_from_glider*256)+
							(unsigned long)(y_lmc_from_glider*256)+
							(unsigned long)(depth_bias*256)+
							(unsigned long)(jitter*256)){
				do{
					gHandle = freopen("/dev/ttyO1","w",gHandle);
				}while(gHandle==NULL);	
				sprintf(dataToGlider,"y\n");
				fputs(dataToGlider,gHandle);
				do{
					gHandle = freopen("/dev/ttyO1","r",gHandle);
				}while(gHandle==NULL);
			}
			else{
				do{
					gHandle = freopen("/dev/ttyO1","w",gHandle);
				}while(gHandle==NULL);	
				sprintf(dataToGlider,"n\n");
				fputs(dataToGlider,gHandle);
				do{
					gHandle = freopen("/dev/ttyO1","r",gHandle);
				}while(gHandle==NULL);
				break;
			}
			
			Xn0 = toDD(Xn0);
			Xe0 = toDD(Xe0);	
			//assume lmc is inited to here
			//in reality this is the diving location?
			//rotate by magdec	
			xhk1=x_lmc_from_glider*cos(magdeclination)+y_lmc_from_glider*sin(magdeclination);
			xhk2=-x_lmc_from_glider*sin(magdeclination)+y_lmc_from_glider*cos(magdeclination);
			x_lmc_from_glider_last = x_lmc_from_glider;	
			y_lmc_from_glider_last = y_lmc_from_glider;	
			//printf("gtan inited with %f,%f,%f,%f,%f,%f\n\r",Xn0,Xe0,x_lmc_from_glider,y_lmc_from_glider,depth_bias,jitter);
			//reinit the priors
			for(int i=0; i < Ns; i++) {
				xkm1[i] = xhk1;
				xkm2[i] = xhk2;
			}
					/*
			//try to handshake the init
			do{
				gHandle = freopen("/dev/ttyO1","w",gHandle);
			}while(gHandle==NULL);			
			checksum = Xn0+Xe0+x_lmc_from_glider+y_lmc_from_glider+depth_bias+jitter;
			sprintf(dataToGlider,"%lf,%lf,%lf,%lf,%lf,%lf %lf tInited\n",Xn0,Xe0,x_lmc_from_glider,y_lmc_from_glider,depth_bias,jitter,checksum);
			fputs(dataToGlider,gHandle);
			do{
				gHandle = freopen("/dev/ttyO1","r",gHandle);
			}while(gHandle==NULL);
			*/
			newData = switchwait;			
			break;
		case switchreset:
		//not implemented yet
		//need to pull particles back in and do other stuff
		
			newData = switchwait;
			break;
		case switchsend:
			//try send the data			
			do{
				gHandle = freopen("/dev/ttyO1","w",gHandle);
			}while(gHandle==NULL);
			checksum = (unsigned long)(sdxToGlider*256)+
						(unsigned long)(sdyToGlider*256)+
						(unsigned long)(Xn0*256)+
						(unsigned long)(Xe0*256)+
						(unsigned long)(((xhk1)*cos(magdeclination)-(xhk2)*sin(magdeclination))*256)+
						(unsigned long)(((xhk1)*sin(magdeclination)+(xhk2)*cos(magdeclination))*256)+
						(unsigned long)(err_pf*256)+ 
						(unsigned long)(index_from_glider*256);
			sprintf(dataToGlider,"%lf %lf %lf %lf %lf %lf %d %lf %lu\r",sdxToGlider,sdyToGlider,Xn0,Xe0,
			((xhk1)*cos(magdeclination)-(xhk2)*sin(magdeclination)),
			((xhk1)*sin(magdeclination)+(xhk2)*cos(magdeclination)),
			err_pf,index_from_glider,checksum);			
			fputs(dataToGlider,gHandle);			
			
			do{
				gHandle = freopen("/dev/ttyO1","r",gHandle);
			}while(gHandle==NULL);
			//grab return string from glider 		
			//must be either good or bad
			do{	
				/*do{
					gHandle = freopen("/dev/ttyO1","w",gHandle);
				}while(gHandle==NULL);
				sprintf(dataToGlider,"y/n?\n");			
				fputs(dataToGlider,gHandle);	
				do{
					gHandle = freopen("/dev/ttyO1","r",gHandle);
				}while(gHandle==NULL);*/
				fgets(command,256,gHandle);
				
			}while(strcmp(dataGood,command)!=0&&strcmp(command,dataBad)!=0);
			
			//if glider got it then go back to waiting
			if(strcmp(dataGood,command)==0){
				newData=switchwait;
			}
			//otherwise try resending it
			else if(strcmp(command,dataBad)==0){
				newData=switchsend;
			}
			
			break;
		default:
			break;
			
			/*
		//if we are renavigating do this one
		if(strcmp(command,commandDataInReNav)==0){
			getGliderData(gHandle);
		   // errcalc = lldist(dataInpos[0],dataInpos[1],Xn0, Xe0);
			errcalc = sqrt(pow(xhk1-dataInpos[0],2)+pow(xhk2-dataInpos[1],2));
			k++;
			errsum = errsum + errcalc;
			//what we send back is the delx and dely to the lmc coordinates
			//printf("%d %.8f %.8f %.8f\n\r ",k,errcalc,xhk1,xhk2);
		    if(getGPSpos(gpspos)==EOF){
				return 1;
				}
			newData=true;		*/	
		}//switch

		




	}//while

}//main


//ptr is a pointer to data from glider
//returns the number of data read in
//should be 6
//returns EOF if end of stream
//should really be calling this anymore

// d./g	out=[x_lmc_free_water y_lmc_free_water alt_to_glider depth_to_glider roll_to_glider pitch_to_glider ];
int getGliderData (FILE *ptr)
{

	int size;

	////printf("in the gHandle\n\r");
    size = fscanf(ptr,"%lf  %lf  %lf  %lf  %lf  %lf  %lf  %lf %lu", &x_lmc_from_glider,
														&y_lmc_from_glider,
														&alt_from_glider,
														&depth_from_glider,
														&roll_from_glider,
														&pitch_from_glider,
														&heading_from_glider,
														&index_from_glider,
														&checksum);


	////printf("read in %d\n\r",size);
    newData=switchwait;

    return size;

}

int getGliderDataSerial (void)
{

    return 0;

}
//ptr is a pointer to gps corrected data
//returns the number of data read in
//should be 3
//returns EOF if end of stream
int getGPSpos (FILE *ptr)
{

	int size;

    size = fscanf(ptr,"%lf  %lf\n", &dataInpos[0],&dataInpos[1]);

    return size;

}

//check if we are within the bounds of the map
bool areweinthemapbounds(double x, double y){

	//check x
	if(x < mapx_min || x > mapx_max){
		return false;
		}
	//else check y
	else if(y > mapy_max || y < mapy_min){
		return false;
		}
	else{//in bounds
		return true;
	}
}


double bilinearmap(double *ptr,double x, double y){
	double  buffer;
	double zI;
	double x1p,y1p,x2p,y2p;

	int mapnx = floor((mapx_max-mapx_min)/mapx_inc)+2;

	double xp=((x-mapx_min)/mapx_inc);
	double yp=((y-mapy_min)/mapy_inc);

	int x1=floor(xp);
	int x2=ceil(xp);
	int y1=floor(yp);
	int y2=ceil(yp);

	buffer=ptr[(y1-1)*mapnx+x1];
	double z11=(double)buffer;
	buffer=ptr[(y2-1)*mapnx+x1];
	double z12=(double)buffer;
	buffer=ptr[(y1-1)*mapnx+x2];
	double z21=(double)buffer;
	buffer=ptr[(y2-1)*mapnx+x2];
	double z22=(double)buffer;
	x1p=mapx_min+floor(xp)*mapx_inc;
	x2p=mapx_min+ceil(xp)*mapx_inc;
	y1p=mapy_min+floor(yp)*mapy_inc;
	y2p=mapy_min+ceil(yp)*mapy_inc;

	if(z11==-1||z12==-1||z21==-1||z22==-1){
		zI=-1;
	}
	else{
		//zI=(1/((x2-x1)*(y2-y1)))*(z11*(x2-x)*(y2-y)+z21*(x-x1)*(y2-y)+z12*(x2-x)*(y-y1)+z22*(x-x1)*(y-y1));
		   zI=(1/((x2p-x1p)*(y2p-y1p)))*(z11*(x2p-x)*(y2p-y)+z21*(x-x1p)*(y2p-y)+z12*(x2p-x)*(y-y1p)+z22*(x-x1p)*(y-y1p));
	}
	return zI;
}


double lldist(double lat1, double lon1,double lat2,double lon2){
	double deltaLat, deltaLon, a, c;
	//float pi = 4*std::atan(1.0);

	deltaLat=lat2*pi/180-lat1*pi/180;
	deltaLon=lon2*pi/180-lon1*pi/180;
	a=pow(sin((deltaLat)/2),2) + cos(lat1)*cos(lat2) * pow(sin(deltaLon/2),2);
	c=2*atan2(sqrt(a),sqrt(1-a));

	return 6378137*c;
}



//What we do
	//1a) compute cumulative sum rk
		//Ns times do 2 and 3
		//2) generate uniform randon number on [0,1]
		//3) find interval in ck that p belongs to


void resample(double *rk, int *idx, int Ns){
	double ck[Ns];
	double p;

	ck[0]=rk[0];
	for(int i=1;i<=Ns;i++){
		ck[i] = ck[i-1] + rk[i];
	}
	ck[Ns]=1;
	for(int j=0;j<Ns;j++){
		p = genrand_real1();

		idx[j]=findp(p,ck,Ns);

	}

}

//find first index such that p > wk[index]
//returns index if success, -1 if fail
int findp(double p, double *wk,int Ns){
	int index;

	//make initial guess based on uniform dist
	index = (int)Ns*p;
	//if(index==0){index=1;}

	//we're bang on
	if(wk[index] == p){
		return index;
	}
	//we still need to go higher
	else if(wk[index] < p){
		for(int i = index;i<Ns;i++){
			if(wk[i] > p){
				return i;
			}
		}
	}
	//we are too high
	else{
		for(int i = index;i>0;i--){
			if(wk[i] < p){
				return i+1;
			}
		}
	}
	return -1;
}

void waitForC(void){
	char c;
	  puts ("c to continue");
	  do {
	    c=getchar();
	    //putchar (c);
  } while (c != 'c');
}

//returns 1 if success 0 if otherwise
//
int raytracedwaterdepth(double roll, double pitch, double heading, double altitude, double depth){
	double dx,dr,dt;
	double z=depth;
	double theta=acos(cos(roll)*cos(pitch));
	double x=0;
	double r=0;
	double t=0;
	double x1=0;
	double a1=0;
	double A1=0;

	double rN = altitude;
	double tN=rN/1500;
	double dz = 0.1;
	if(ceil(z)-floor(z)==0){
		z=z+0.0001;
	}
	double c = svelBuffer[(int)floor(z)]+(svelBuffer[(int)ceil(z)]-svelBuffer[(int)floor(z)])*((z-floor(z))/(ceil(z)-floor(z)));
	double a = sin(theta/c);
	////printf("%.8f %.8f %.8f \n\r",theta,c,a);

	while(t<tN){
		z=z+dz;
		dx=dz*tan(theta);
		dr=sqrt(pow(dx,2)+pow(dz,2));
		dt=dr/c;
		x=x+dx;
		r=r+dr;
		t=t+dt;
		if(ceil(z)-floor(z)==0){
			z=z+0.0001;
		}
		c=svelBuffer[(int)floor(z)]+(svelBuffer[(int)ceil(z)]-svelBuffer[(int)floor(z)])*((z-floor(z))/(ceil(z)-floor(z)));
		theta=asin(a*c);
	}
		//compute adjustment locations
	/*
	x1 = (water_depths(k)-m_depth(ap(iap(k))))*tan((m_pitch(ap(iap(k)))+26)*pi()/180);
	a1 = sqrt(2*m_altitude(ap(iap(k)))^2-2*m_altitude(ap(iap(k)))^2*cos((m_roll(ap(iap(k))))*pi()/180));

	%x1=(((m_roll(ap(iap(k))))*pi()/180)
	%a1=(((m_roll(ap(iap(k))))*pi()/180)*m_altitude(ap(iap(k))));
	%x1=(((m_pitch(ap(iap(k)))+26)*pi()/180)*m_altitude(ap(iap(k))));     
	A1=atan2(a1,x1);

	xp(k)=x_adj(k).*sin(heading(ap(iap(k)))+headingcor+A1);
	yp(k)=x_adj(k).*cos(heading(ap(iap(k)))+headingcor+A1);
	*/
	x1 = (raytracedz-depth)*tan(pitch);
	a1 = sqrt(2*pow(altitude,2)-2*pow(altitude,2)*cos(roll));
	A1 =atan2(a1,x1);
	raytracedx_off=x*sin(heading+magdeclination+A1);
	raytracedy_off=x*cos(heading+magdeclination+A1);
	
	if((z!=0) & (x!=0)){
		//raytracedx = x;
		raytracedz = z;
		return 1;
	}
	return 0;
}

double toDD(double latorlon){
	double getminutes =fmod(latorlon,100);
	double getdegrees = (latorlon - getminutes)/100;
	

	return getdegrees + getminutes/60;
}