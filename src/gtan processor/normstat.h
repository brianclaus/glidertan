

#ifndef __NORMSTAT_H
#define __NORMSTAT_H
#include <math.h>

double const pi = 4*atan(1.0);
 //double const pi = 3.141592653589793;


// simple function for normal distribution
double normpdf(double x, double m, double s);  //returns f(x|m,s)


//Generates a normally distributed random number
//taken from http://rosettacode.org/wiki/Random_numbers#C.2B.2B
//on 21/02/2013
//by Brian Claus
double  normrand(double m, double s);


#endif