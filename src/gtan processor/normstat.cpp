  #include "normstat.h"
  #include "mtrand.h"





double normpdf(double x, double m, double s)
{

    return (1/(s*sqrt(2*pi)))*exp(-pow((x-m),2)/(2*pow(s,2)));

}

double normrand(double m, double s){

    double r1 = genrand_real1(); // gives equal distribution in [0, 1]
    double r2 = genrand_real1(); // gives equal distribution in [0, 1]
    return m + s * sqrt(-2*log(r1))*cos(2*pi*r2);
}

