#ifndef __COMMON_H
#define __COMMON_H


//file names and variables
char mapname[] =  "hroodgridmap2";
char mapconstantsname[] = "mapconstants2";
char svelname[] = "sveltobbb";
ulong lSize;
double * mapBuffer;
double svelBuffer[221];
size_t result;

//vehicle parameters
double altimeter_pitch_offset = 26;
double altimeter_distance =0.762;
double depth_distance =0.075;

//to correct we do a clockwise rotation for LMC to EARTH
//when sending back we do a ccw rotation for EARTH to LMC
double magdeclination = 0.314;

//raytrace variables
double raytracedz=0;
double raytracedx_off=0;
double raytracedy_off=0;


//variables from glider
double x_lmc_from_glider;
double y_lmc_from_glider;
double alt_from_glider;
double depth_from_glider;
double roll_from_glider;
double pitch_from_glider;
double heading_from_glider;
double index_from_glider;

//variables to glider
double sdxToGlider;
double sdyToGlider;


//computed from glider
double water_depthe;
double sdx;
double sdy;
double sdxR;
double sdyR;
double x_lmc_from_glider_last=0;
double y_lmc_from_glider_last=0;

//particle filter variables
const int Ns =1000;
double xhk1,xhk2,Xe0,Xn0;
double xhk1m1,xhk2m1,Xn0m1,Xe0m1;
double xk1[Ns],xk2[Ns];
double xkm1[Ns],xkm2[Ns];
double lato[Ns], lono[Ns];
double covlonlon=0,covlatlat=0,covlatlon=0;
double covx1x1=0,covx2x2=0,covx1x2=0;
double p_zk_given_xk[Ns];
int resample_index[Ns];
double wk[Ns];
double po;
double depth_bias =1;
double jitter =20;
double mapvariance =0.5;
unsigned long checksum=0;

//Map variables
double mapy_min =47.387999999999998;
double mapy_max =47.454000000000001;
double mapx_max =-53.104999999999997;
double mapx_min =-53.140000000000001;
double mapx_inc =2.658160000000000e-005;
double mapy_inc =1.798560000000000e-005;


double dataInpos[2]={0,0};

enum switch_t {switchrun=1,
	switchinit,
	switchreset,
	switchsend,
	switchwait};
	
switch_t newData = switchwait;


//commands
//All commands should be three chars plus \n
char command [4];
char commandReset[] = "res\n";
char commandDataIn[] = "din\n";
char commandInit[] = "ini\n";
char dataGood[] = "y\n";
char dataBad[] = "n\n";



#endif