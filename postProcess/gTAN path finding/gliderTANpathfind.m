%TODO
%Test out a water velocity bias estimator
%Test out a depth bias estimator
clear all

%load map
gtan.maps=2;
gtan=gtanloadMap(gtan,sprintf('../hroodgridmap%d.mat',gtan.maps));
gtan.Ns=400;


%starting locations
glider.iap=1;
glider.loni=-53.131144462516687;
glider.lati=47.392674449933487;

%end location
glider.lone = -53.118219515555410;
glider.late = 47.451393274867371;

%search radius [m]
glider.radius = 1000;
%search angle [deg]
glider.angle = 60;

glider.water_vx=0.05;
glider.water_vy=-0.02;

glider.klastPath = 0;
glider.klastTAN = 0;

%first go
glider = gtanFindPaths(glider,gtan);
for j = 1:length(glider.wptflag)
     if(glider.wptflag(j)==1)
        [glider,gtan]=gtanCrunchPaths(glider,gtan,j,0);
        display(sprintf('%d rmse %f[m]',length(gtan.rmse),gtan.rmse(end)));
     end
end
%find best wpt and save priors
glider.lastWpt = find(gtan.rmse==min(gtan.rmse));
display(sprintf('select %d rmse %f[m]',glider.lastWpt,gtan.rmse(glider.lastWpt)));
[glider,gtan]=gtanCrunchPaths(glider,gtan,glider.lastWpt,1);
display(sprintf('post recrunch %d rmse %f[m]',length(gtan.rmse),gtan.rmse(end)));
while(gtan.rmse(end)>gtan.rmse(glider.lastWpt)*1.1)
    [glider,gtan]=gtanCrunchPaths(glider,gtan,glider.lastWpt,1);
    display(sprintf('post recrunch %d rmse %f[m]',length(gtan.rmse),gtan.rmse(end)));
end    


 figure(1)
    clf
    hold on
for j = 1:length(glider.wptflag)
    if(glider.wptflag(j)==1)
        
        scatter(glider.lon(j,:),glider.lat(j,:),30,-(glider.alt(j,:)+glider.z(j,:)),'.')
        axis equal
    end
end

disttowpt = gtanlldist([glider.yi(end);glider.xi(end)],[glider.late;glider.lone]);
 
while(disttowpt > glider.radius)
    %init next segment of paths
    glider.loni=glider.lon(glider.lastWpt,end);
    glider.lati=glider.lat(glider.lastWpt,end);
    glider.klastPath = length(glider.xi);
    glider.klastTAN = length(gtan.ia);



    %subsequent go
    glider = gtanFindPaths(glider,gtan);
    gtan=rmfield(gtan,'rmse');

     figure(1)
    for j = 1:length(glider.wptflag)
        if(glider.wptflag(j)==1)

            scatter(glider.lon(j,glider.klastPath+1:end),glider.lat(j,glider.klastPath+1:end),30,-(glider.alt(j,glider.klastPath+1:end)+glider.z(j,glider.klastPath+1:end)),'.')
            axis equal
        end
    end

    for j = 1:length(glider.wptflag)
         if(glider.wptflag(j)==1)
            [glider,gtan]=gtanCrunchPaths(glider,gtan,j,0);
            display(sprintf('%d rmse %f[m]',length(gtan.rmse),gtan.rmse(end)));
         end
    end
    %find best wpt and save priors
    glider.lastWpt = find(gtan.rmse==min(gtan.rmse),1);
    display(sprintf('select %d rmse %f[m]',glider.lastWpt,gtan.rmse(glider.lastWpt)));
    [glider,gtan]=gtanCrunchPaths(glider,gtan,glider.lastWpt,1);
    display(sprintf('post recrunch %d rmse %f[m]',length(gtan.rmse),gtan.rmse(end)));
    while(gtan.rmse(end)>gtan.rmse(glider.lastWpt)*1.1)
        [glider,gtan]=gtanCrunchPaths(glider,gtan,glider.lastWpt,1);
        display(sprintf('post recrunch %d rmse %f[m]',length(gtan.rmse),gtan.rmse(end)));
    end  
    disttowpt = gtanlldist([glider.yi(end);glider.xi(end)],[glider.late;glider.lone]);
    display(sprintf('dist left %f[m] rmse %f[m]',disttowpt,gtan.rmse(glider.lastWpt)));
end

