%% GTAN TODO
%  - cleanup raytracing
%  - investigate depth bias estimator

clear all
load dataprocdec4


i=836:836+5700;


%% get variables
glider = gtanvariables(data,sensor_lookup,i);

%% correct glider deadreckoning using surfacing locations
glider = gtancorrectGPS(glider);


 %% altitude corrections
 %consider removing ray tracing?
 %attempt correction only by mean water velocity or by binned water
 %velocity
 %%need some function that updates water velocities based previous glider
 %%profile
 load soundvelocity
load waterdensity
glider = gtancorrectAltitudes(glider,svelc,densa);
glider.water_depthr = glider.water_depth(glider.ia)'; 

%% crunch the algorithm
gtan.T = 150;   % number of time steps
gtan.Nmc = 1;     %number of monte-carlo runs


gtan.maps=2;     %maps can be 2,4,10,20,40,60,80,100
gtan.Ns = 1000;    %number of particles, can be a range eg. 100:100:1000;
gtan.R=15;     %jittering variance, can be a range eg. 10:2:16

% gtan.errkm=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,T);
% gtan.errkmdr=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,T);
% gtan.cloudsize=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,2,T);
% gtan.particlecov=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,2,2,T);

gtan=gtanloadMap(gtan,sprintf('hroodgridmap%d',gtan.maps));





glider.hhzkblp = zeros(1,length(glider.xi(glider.iap)));
glider.hhzkblphr = zeros(1,length(glider.xi(glider.iap)));

for k = 1:length(glider.xi(glider.iap))  

        if(gtaninbounds(glider.xi(glider.iap(k)),glider.yi(glider.iap(k)),gtan))

           zI=gtanbilinearint(glider.xi(glider.iap(k)),glider.yi(glider.iap(k)),gtan);
           glider.hhzkblp(k) =zI;
        else
            glider.hhzkblp(k) = -99;
        end
end

%can we include a depth bias estimate?
gtan.depthbiasg = mean(glider.water_depthg(1:120)-glider.hhzkblp(1:120));
gtan.depthbiasr = mean(glider.water_depthr(1:120)-glider.hhzkblp(1:120));
gtan.depthbiass = mean(glider.water_depths(1:120)-glider.hhzkblp(1:120));
gtan.depthbiasw = mean(glider.water_depthw(1:120)-glider.hhzkblp(1:120));
gtan.depthvarg = std(glider.water_depthg(1:120)-glider.hhzkblp(1:120));
gtan.depthvarr = std(glider.water_depthr(1:120)-glider.hhzkblp(1:120));
gtan.depthvars = std(glider.water_depths(1:120)-glider.hhzkblp(1:120));
gtan.depthvarw = std(glider.water_depthw(1:120)-glider.hhzkblp(1:120));



%minus the water depth bias
gtan.water_depthe=glider.water_depthw-gtan.depthbiasw;
   
%% setup variables
    gtan.Xe0dr = (glider.xi(glider.iap(1)));
    gtan.Xn0dr = (glider.yi(glider.iap(1)));
    gtan.Xe0=(glider.xi(glider.iap(1)));
    gtan.Xn0=(glider.yi(glider.iap(1)));

     gtan.xk = zeros(2,gtan.Ns);
     gtan.xkm1 = randn(2,gtan.Ns)*0;
     gtan.xhk = zeros(2,gtan.T);
     gtan.xhkdr = zeros(2,gtan.T);
     
     gtan.distu = 0;
     gtan.var = zeros(2,gtan.T);
     gtan.gvel = zeros(1,gtan.T);
     gtan.gtanvel = zeros(1,gtan.T);
     gtan.gtanhead = zeros(1,gtan.T);
     gtan.drerrrate = ones(1,gtan.T).*0.25;
     gtan.p_zk_given_xk = zeros(1,gtan.Ns);
     gtan.wkm1 = zeros(1,gtan.Ns);
     gtan.lato = zeros(1,gtan.Ns);
     gtan.lono = zeros(1,gtan.Ns);
    gtan.wk=zeros(gtan.Ns,1)';
  
    
    gtan.map=0;
    %set to 2 to ded-reckon 0 to terrain nav
    gtan.ded=0;
    gtan.mapbounds=zeros(1,gtan.T);
    gtan.shoreline=zeros(1,gtan.T);

%% for all altimeter readings process gliderTAN
for k =2:gtan.T    

    %zero our estimates for this k
    gtan.xhk(:,k) = zeros(2,1);

    gtan.Xn0(k)=0;
    gtan.Xe0(k) =0;

    [glider, gtan] = gtancomputesdxandy(glider,gtan,k);      

    [glider,gtan]=gtancomputeParticlesandWeights(glider,gtan,k);

    [glider, gtan] = gtancomputeEstimates(glider,gtan,k);


end
figure(14)
clf
plot(gtan.gvel,'b')   
hold on
plot(gtan.gtanvel,'r')

gtan.gtanhead=atan2(gtan.xdel(2,:),gtan.xdel(1,:));
figure(15)
clf
plot(gtan.gtanhead,'b')   
hold on
plot(atan2(sin(glider.heading(glider.ia)+glider.headingcor),cos(glider.heading(glider.ia)+glider.headingcor)),'r')
plot(glider.heading(glider.ia)+glider.headingcor,'g')


figure(16)
clf
plot(gtan.distu(1:k),squeeze(gtan.errkm(1:k)))
 hold on
plot(gtan.distu(1:k),squeeze(gtan.errkmdr(1:k)),'r')
plot(gtan.distu(1:k),sqrt(gtan.cloudsize(1,1:k)),'g')

figure(6)
clf
h = axes;
hold on
plot((glider.xi(glider.iap(1:1+length(gtan.xhk(1,:))))),(glider.yi(glider.iap(1:1+length(gtan.xhk(1,:))))),'x-r','Markersize',4,'Linewidth',1)

plot(gtan.Xe0,gtan.Xn0,'x-k','Markersize',4,'Linewidth',1)
plot(gtan.Xe0dr,gtan.Xn0dr,'x-b','Markersize',4,'Linewidth',1)
for i =2:gtan.T
    if(eig(squeeze(gtan.particlecov(:,:,i)))>0)
        he = gtanerror_ellipse(squeeze(gtan.particlecov(:,:,i)),[gtan.Xe0(i) gtan.Xn0(i)]);
        set(he,'color','g')
    end
end
grid on
box on
axis equal
legend('Glider','SIR','DR','Location','SouthEAST','Orientation','horizontal')
xlabel('Lon [deg]')  
ylabel('Lat [deg]')

gtan.rmse = mean(gtan.errkm(1:120));
display(sprintf('map:%d N:%d R:%d rmse:%f',gtan.maps,gtan.Ns,gtan.R,gtan.rmse))
%         
% end
% end
% end



