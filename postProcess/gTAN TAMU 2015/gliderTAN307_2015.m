%% GTAN TODO
%  - eliminate unecessary variables
%  - move code chunks into functions
%  - consider simplifying raytracing
%  - move variables into structs where possible
%  - investigate depth bias estimator
%  - jitter variance should be variable

clear all
load gtanDEM
load glider307_2015



%% correct glider deadreckoning using surfacing locations
glider = gtancorrectGPS(glider);

%create water velocity function



 %% ray tracing
 %consider removing ray tracing?
 %attempt correction only by mean water velocity or by binned water
 %velocity
 load soundvelocity
load waterdensity
glider = correctAltitudes(glider,svelb,densa);
glider.water_depthr = glider.water_depth(glider.ia)'; 

%% crunch the algorithm
gtan.T = 150;   % number of time steps
gtan.Nmc = 1;     %number of monte-carlo runs


gtan.maps=4;     %maps can be 2,4,10,20,40,60,80,100
gtan.Ns = 1000;    %number of particles, can be a range eg. 100:100:1000;
gtan.R=15;     %jittering variance, can be a range eg. 10:2:16

% gtan.errkm=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,T);
% gtan.errkmdr=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,T);
% gtan.cloudsize=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,2,T);
% gtan.particlecov=zeros(length(gtan.Nsjj),length(gtan.Rjj),gtan.Nmc,2,2,T);

gtan=loadGtanMap(gtan,sprintf('hroodgridmap%d',gtan.maps));





glider.hhzkblp = zeros(1,length(glider.xi(glider.iap)));
glider.hhzkblphr = zeros(1,length(glider.xi(glider.iap)));

for k = 1:length(glider.xi(glider.iap))  

        if(gtaninbounds(glider.xi(glider.iap(k)),glider.yi(glider.iap(k)),gtan))

           zI=gtanbilinearint(glider.xi(glider.iap(k)),glider.yi(glider.iap(k)),gtan);
           glider.hhzkblp(k) =zI;
        else
            glider.hhzkblp(k) = -99;
        end
end
gtan.depthbiasg = mean(glider.water_depthg(1:120)-glider.hhzkblp(1:120));
gtan.depthbiasr = mean(glider.water_depthr(1:120)-glider.hhzkblp(1:120));
gtan.depthbiass = mean(glider.water_depths(1:120)-glider.hhzkblp(1:120));

   
%% setup variables
    gtan.Xe0dr = (glider.xi(glider.iap(1)));
    gtan.Xn0dr = (glider.yi(glider.iap(1)));
    gtan.Xe0=(glider.xi(glider.iap(1)));
    gtan.Xn0=(glider.yi(glider.iap(1)));
                                   % number of particles

     gtan.xk = zeros(2,gtan.Ns);
     gtan.xkm1 = randn(2,gtan.Ns)*0;
     gtan.xhk = zeros(2,gtan.T);
     gtan.xhkdr = zeros(2,gtan.T);
     
     gtan.var = zeros(2,gtan.T);
     gtan.p_zk_given_xk = zeros(1,gtan.Ns);
     gtan.wkm1 = zeros(1,gtan.Ns);
     gtan.lato = zeros(1,gtan.Ns);
     gtan.lono = zeros(1,gtan.Ns);
    gtan.wk=zeros(gtan.Ns,1)';
  
    
    gtan.map=0;
    %set to 2 to ded-reckon 0 to terrain nav
    gtan.ded=0;
    gtan.mapbounds=zeros(1,gtan.T);
    gtan.shoreline=zeros(1,gtan.T);

    %% for all altimeter readings process gliderTAN
    for k =2:gtan.T    
        gtan.DT(k) = glider.time(glider.ia(k))-glider.time(glider.ia(k-1));
                    
            %zero our estimates for this k
            gtan.xhk(:,k) = zeros(2,1);
           
            gtan.Xn0(k)=0;
            gtan.Xe0(k) =0;

                
        %normal ie. no surfacing between alt readings
        %extract sdx, sdy function here
        if(glider.i_end(find(glider.i_end>glider.ia(k-1),1))>glider.ia(k))

            iv=glider.ia(k-1)+1:glider.ia(k);
            iv=iv(find(~isnan(glider.speed(iv))));
            dt = glider.time(iv)-glider.time(iv-1);
            dx =(glider.speed(iv).*sin(glider.heading(iv)+glider.headingcor).*dt);
            dy =(glider.speed(iv).*cos(glider.heading(iv)+glider.headingcor).*dt);

            gtan.sdx=sum(dx);
            gtan.sdy=sum(dy);
            vm=sum(glider.speed(iv).*dt)/sum(dt);
            bearingm = atan2(gtan.sdx,gtan.sdy);
            gtan.sdxu=gtan.sdx;
            gtan.sdyu=gtan.sdy;
            %if there was a surfacing
        else
            igps1=glider.i_end(find(glider.i_end>glider.ia(k-1),1));
            igps2=glider.i_start(find(glider.i_start>glider.ia(k-1),1));
            iv= [glider.ia(k-1)+1:igps1 igps2:glider.ia(k)];
            iapend = glider.iap(k)-(glider.ia(k)-igps2);
            iapc2=(find(glider.ap>igps1,1)-1);
            iapc1=(find(glider.ap>igps2,1)-1);
            DT(k)=glider.time(glider.ia(k))-glider.time(igps2);
            iv=iv(find(~isnan(glider.speed(iv))));
            dt = glider.time(iv)-glider.time(iv-1);
            dx =(glider.speed(iv).*sin(glider.heading(iv)+glider.headingcor).*dt);
            dy =(glider.speed(iv).*cos(glider.heading(iv)+glider.headingcor).*dt);
           

            sdx=sum(dx);
            sdy=sum(dy);
            vm=sum(glider.speed(iv).*dt)/sum(dt);
            bearingm = atan2(sdx,sdy);

             [xsurf,ysurf]=lldistkm([glider.yi(iapc2) glider.xi(iapc2) ],[(glider.yi(iapc1)) (glider.xi(iapc1)) ]);
             %we need to update last location with surface drift so our
             %computations don't break
            gtan.sdxu=sdx;
            gtan.sdyu=sdy;
            gtan.sdx=sum(dx) + xsurf;
            gtan.sdy=sum(dy) + ysurf;
        end
            
        %minus the water depth bias
        %water_depthe(k)=water_depths(k)-tideg(ap(iap(k)))-2.746;
        gtan.water_depthe(k)=glider.water_depths(k)-gtan.depthbiass;

                    
       

       
        %% Algorithm 3 of Ref [1]
       [glider,gtan]=computeParticlesandWeights(glider,gtan,k);
  
        %%checkum function here
        %we must have good particles
        if(gtan.map==gtan.ded) 
            %sum up the particle weights
            t=sum(gtan.p_zk_given_xk);

            %normalize them
            gtan.wk = gtan.p_zk_given_xk./gtan.po;


                %xkm1=xk;
               [gtan.xkt, gtan.wk, gtan.idx]=resample(gtan.xk,gtan.wk','beaglesample'); 
               

            %calc xhk
            %xhk(:,k)=[sum(xk(1,:).*wk);sum(xk(2,:).*wk)];
            for ig = 1:gtan.Ns
               gtan.xhk(1,k)=gtan.xhk(1,k)+gtan.xk(1,gtan.idx(ig))*(1/gtan.Ns);
               gtan.xhk(2,k)=gtan.xhk(2,k)+gtan.xk(2,gtan.idx(ig))*(1/gtan.Ns);
               gtan.xkm1(1,ig)=gtan.xk(1,gtan.idx(ig));
               gtan.xkm1(2,ig)=gtan.xk(2,gtan.idx(ig));
               
            end
                       
            
            gtan.Xn0(k)=gtan.Xn0(k-1)+((gtan.xhk(2,k)-gtan.xhk(2,k-1))/6376669.0)*180/pi();
            gtan.Xe0(k)=gtan.Xe0(k-1)+((gtan.xhk(1,k)-gtan.xhk(1,k-1))/(6376669.0*cosd(gtan.Xn0(k-1))))*180/pi();    
            gtan.xhkdr(:,k)=[gtan.xhkdr(:,k-1)+[gtan.sdx;gtan.sdy]];
            gtan.Xn0dr(k) = gtan.Xn0dr(k-1)+(gtan.sdy/6378137.0)*180/pi();
            gtan.Xe0dr(k) = gtan.Xe0dr(k-1)+(gtan.sdx/(6378137.0*cosd(gtan.Xn0dr(k-1))))*180/pi();
            gtan.wkm1=gtan.wk;
            gtan.var(:,k) = sqrt((gtan.xhk(:,k)-[gtan.xhk(:,k-1)+[gtan.sdxu;gtan.sdyu]]).^2);
            gtan.xdel(:,k) = sqrt((gtan.xhk(:,k)-gtan.xhk(:,k-1)).^2)./gtan.DT(k);
       
            gtan.errkm(k) = lldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0(k) gtan.Xe0(k) ]);
            gtan.errkmdr(k) = lldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0dr(k) gtan.Xe0dr(k) ]);
            gtan.cloudsize(:,k) = mean([sum(sqrt((gtan.xhk(1,k)-gtan.xk(1,gtan.idx)).^2)); sum(sqrt((gtan.xhk(2,k)-gtan.xk(2,gtan.idx)).^2))]);
            gtan.particlecov(:,:,k)=cov(gtan.lono(gtan.idx),gtan.lato(gtan.idx));
           
        else
               % disp(sprintf('Ded reckoning %d...',k))
               gtan.mapbounds(k)=1;
               gtan.xhk(:,k) = [gtan.xhk(:,k-1)+[gtan.sdx;gtan.sdy]];
               gtan.Xn0(k) = gtan.Xn0(k-1)+(gtan.sdy/6378137.0)*180/pi();
               gtan.Xe0(k) = gtan.Xe0(k-1)+(gtan.sdx/(6378137.0*cosd(gtan.Xn0(k-1))))*180/pi();
               gtan.xhkdr(:,k)=[gtan.xhkdr(:,k-1)+[gtan.sdx;gtan.sdy]];
               gtan.Xn0dr(k) = gtan.Xn0dr(k-1)+(gtan.sdy/6378137.0)*180/pi();
               gtan.Xe0dr(k) = gtan.Xe0dr(k-1)+(gtan.sdx/(6378137.0*cosd(gtan.Xn0dr(k-1))))*180/pi();

               gtan.xkm1 = [gtan.xhk(1,k)+randn(1,gtan.Ns).*20;gtan.xhk(2,k)+randn(1,gtan.Ns).*20;];
                gtan.errkm(k) = lldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0(k) gtan.Xe0(k) ]);
                gtan.errkmdr(k) = lldist([(glider.yi(glider.iap(k))) (glider.xi(glider.iap(k)))],[ gtan.Xn0dr(k) gtan.Xe0dr(k) ]);
                gtan.var(:,k) = sqrt((gtan.xhk(:,k)-[gtan.xhk(:,k-1)+[gtan.sdxu;gtan.sdyu]]).^2);
                gtan.xdel(:,k) = sqrt((gtan.xhk(:,k)-gtan.xhk(:,k-1)).^2)./gtan.DT(k);
                %check what the deal is here with the cov
                gtan.cloudsize(:,k) = mean([sum(sqrt((gtan.xhk(1,k)-gtan.xk(1,gtan.idx)).^2)); sum(sqrt((gtan.xhk(2,k)-gtan.xk(2,gtan.idx)).^2))]);
                gtan.particlecov(:,:,k)=cov(gtan.lono(gtan.idx),gtan.lato(gtan.idx));

            gtan.map=3;
        end


    end
    

figure(16)
clf
plot(1:k,squeeze(gtan.errkm(1:k)))
 hold on
plot(1:k,squeeze(gtan.errkmdr(1:k)),'r')
plot(1:k,sqrt(gtan.cloudsize(1,1:k)),'g')

figure(6)
clf
h = axes;
hold on
plot((glider.xi(glider.iap(1:1+length(gtan.xhk(1,:))))),(glider.yi(glider.iap(1:1+length(gtan.xhk(1,:))))),'x-r','Markersize',4,'Linewidth',1)

plot(gtan.Xe0,gtan.Xn0,'x-k','Markersize',4,'Linewidth',1)
plot(gtan.Xe0dr,gtan.Xn0dr,'x-b','Markersize',4,'Linewidth',1)
for i =2:gtan.T
    if(eig(squeeze(gtan.particlecov(:,:,i)))>0)
        he = error_ellipse(squeeze(gtan.particlecov(:,:,i)),[gtan.Xe0(i) gtan.Xn0(i)]);
        set(he,'color','g')
    end
end
grid on
box on
axis equal
legend('Glider','SIR','DR','Location','SouthEAST','Orientation','horizontal')
xlabel('Lon [deg]')  
ylabel('Lat [deg]')
colorbar

gtan.rmse = sum(gtan.errkm(1:124))/(k-1);
display(sprintf('map:%d N:%d R:%d rmse:%f',gtan.maps,gtan.Ns,gtan.R,gtan.rmse))
%         
% end
% end
% end



